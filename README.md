# A Nice Day

## Notes

Thank you TWC for the opportunity!

For the task at hand, admittedly, this application is a _bit_ over engineered. The reason being is that I wanted to show as many of the Vue features along with other parts of the ecosystem such as Vuex and Vue Router that I could within this task.

Setup and build instructions are below :)

Please let me know if you have any questions.
~Brad

## Project setup

```
yarn
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

#### \*\*If you choose this option you will need to distribute these files with a server. See output console for more info.

```
yarn build
```

### Lints and fixes files

```
yarn lint
```
