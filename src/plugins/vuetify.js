import Vue from 'vue';
import Vuetify, {
  VApp,
  VDialog,
  VSelect,
  VIcon
} from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {
  iconfont: 'md',
  components: {
    VApp,
    VDialog,
    VSelect,
    VIcon
  },
  theme: {
    primary: '#FFA500'
  }
});