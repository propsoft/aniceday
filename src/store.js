import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    settings: {
      location: null,
      niceDayType: null
    }
  },
  getters: {
    location: state => {
      return state.settings.location
    },
    niceDayType: state => {
      return state.settings.niceDayType
    },
  },
  mutations: {
    setLocation(state, location) {
      state.settings.location = location
    },
    setNiceDayType(state, type) {
      state.settings.niceDayType = type
    }
  },
  actions: {
    setLocation({
      commit
    }, payload) {
      commit('setLocation', payload)
    },
    setNiceDayType({
      commit
    }, payload) {
      commit('setNiceDayType', payload)
    },
  }
});